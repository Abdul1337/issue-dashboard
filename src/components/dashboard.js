import React from 'react';
import 'antd/dist/antd.css';
import './main.css'
// Antd Components
import { Table,Button, message,Tooltip} from 'antd';
// Antd Icons
import { DownloadOutlined,UploadOutlined, EllipsisOutlined } from '@ant-design/icons';
// Utility Functions
import { sortByKey, parseJSONtoStructuredData, getIssueCount, getUserCount, getVersionCount,} from './Utils'
// Components
import DisplayIssues from './DisplayIssues'



class Dashboard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            jsonUploaded: false,
            structuredData : localStorage.getItem('uploadedFileContent') ? parseJSONtoStructuredData(JSON.parse(localStorage.getItem('uploadedFileContent'))) : null,
        };
    }

    // Upload & Fetch JSON
    handleUpload(e,ref) {
        const files = document.getElementById('fileIN').files;
        if (files.length <= 0) {
            message.error('No Files Were Uploaded');

        } else {
            message.success('File Uploaded Successfully!');
            var reader = new FileReader();
            reader.onload = function (e) {
                var result = JSON.parse(e.target.result);
                ref.updateState(result);
                console.log("State Updated");
                // localStorage.setItem('uploadedFileContent',JSON.stringify(result));
            }
            reader.readAsText(files.item(0));
        }
    }

    // Update State with Raw data + Structured Data
    updateState(data){
        if(data.length){
            // console.log("data Outer" , data);
            this.setState({
                structuredData : parseJSONtoStructuredData(data),
                jsonUploaded : true,
                rawData : data,
            })
        }
    }
    
    colTable1 = [
        {title: 'S.N.', dataIndex: 'key',key: 'key',render: sn => <strong>{sn.toString()}</strong>,},
        {title: 'Error Group', dataIndex: 'errGroup',key: 'errGroup',width : 200,sorter : sortByKey.bind({key :'errGroup'}),},
        {title: 'Error Message', dataIndex: 'errMsg',key: 'errMsg',width : 400,
        render: msg => <i style={{cursor : 'pointer'}} >{msg.toString().trim().length > 35 ? shortenTextAsTooltip(msg,35) : msg.toString().trim()}</i>,

    },
        {title: 'App Version/Code', dataIndex: 'appVerCode',key: 'appVerCode',width : 150,sorter : sortByKey.bind({key :'appVerCode'}),},
        {title: 'School', dataIndex: 'school',key: 'school',sorter : sortByKey.bind({key :'school'}),render: schoolName => <span>{schoolName.toString().split("# ")[1]}</span>,},
        {title: 'User Name', dataIndex: 'usrName',key: 'usrName',width : 250,sorter : sortByKey.bind({key :'usrName'}),render: userName => <span><strong>{userName.toString().split('|')[0]}</strong><br/>[{userName.toString().split('|')[1]}]</span>,},
    ];

    render() {
    console.log("Data Uploaded : ", this.state.rawData);
    console.log("Data Parsed : ",this.state.structuredData);
      return (
        <div className="container" >
            {
                this.state.jsonUploaded?
                <div className='tableContainer'>
                    <StatsCards data={this.state.structuredData}/>
                    <DisplayIssues title={"Top Issues"} data={this.state.structuredData}/>
                    <Displaydata title={"Submissions"} data={this.state.structuredData} columns={this.colTable1}/>
                    <Downloaddata filename='issue_data' data={this.state.rawData}/>
                </div>:
                <div className='dataUploader'>
                    <label className="uploadInput" htmlFor ="fileIN">
                        <UploadOutlined style={{fontSize:"40px"}}/>
                        <span>Choose a File</span>
                    </label>
                    <input  style={{display : "none"}} type="file" id="fileIN" value="" accept="application/JSON" onChange={(e)=>{this.handleUpload(e,this);}} />
                </div>
            }
        </div>
      )
    }
}



const shortenTextAsTooltip = (msg,len) =>{
    return <Tooltip title={msg} color='blue'>{msg.trim().slice(0,len)}<EllipsisOutlined /></Tooltip>
}


// Components





class StatsCards extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            stats_submissions : this.props.data.length,
            stats_issues : getIssueCount(this.props.data).length,
            stats_users : getUserCount(this.props.data).length,
            stats_versions : getVersionCount(this.props.data).length,
        };
    } 

render(){
    // console.log("Props : ",this.props)
    // console.log("Data : ",this.props.data)
    
    return(
        <div className='dataStats'> 
            <div className="statsItem">
                <div className="statsItem--value">{this.state.stats_submissions}</div>
                <div className="statsItem--name">Submission{this.state.stats_submissions > 1 ? "s" : ""}</div>
            </div>
            <div className="statsItem">
                <div className="statsItem--value">{this.state.stats_issues}</div>
                <div className="statsItem--name">Issue{this.state.stats_issues > 1 ? "s" : ""}</div>
            </div>
            <div className="statsItem">
                <div className="statsItem--value">{this.state.stats_users}</div>
                <div className="statsItem--name">User{this.state.stats_users > 1 ? "s" : ""}</div>
            </div>
            <div className="statsItem">
                <div className="statsItem--value">{this.state.stats_versions}</div>
                <div className="statsItem--name">App Version{this.state.stats_versions > 1 ? "s" : ""}</div>
            </div>
        </div>
    )
}

}
function Displaydata(props) {
    return(
        <div>
            <div className="TableTitle">{props.title}</div>
            <div className='dataDisplay'>
                <Table 
                    dataSource={props.data} 
                    columns={props.columns}
                    pagination={{ pageSize: 100 }} 
                    scroll={{ y: 500 }} 
                />
            </div>
        </div>
    )
}

function Downloaddata(props) {
    return(
        <div className='dataDownload'> 
                 <Button 
                    type="primary" 
                    icon={<DownloadOutlined />} 
                    size='large'
                    onClick = {()=>downloadObjectAsJson(props.data,props.filename)}
                > 
                    Download JSON
                </Button>
        </div>   
    )
}

function downloadObjectAsJson(exportObj, exportName){
    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(exportObj));
    var downloadAnchorNode = document.createElement('a');
    downloadAnchorNode.setAttribute("href",     dataStr);
    downloadAnchorNode.setAttribute("download", exportName + ".json");
    document.body.appendChild(downloadAnchorNode); // required for firefox
    downloadAnchorNode.click();
    downloadAnchorNode.remove();
  }

// downloadObjectAsJson(structuredData,'data2')





export default Dashboard;