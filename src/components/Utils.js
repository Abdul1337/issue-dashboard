// Utils

export const sortByKey = function(a,b) {
    let key = this.key;
    return a[key].toUpperCase() < b[key].toUpperCase() ? -1 : (a[key].toUpperCase() > b[key].toUpperCase() ? 1 : 0)
}

export const getIndexByColName = (cn)=>{
    let i = 0;
    switch(cn){
        case 'appVerCode':
            i = 2;
            break;
        case 'errGroup':
            i = 0;
            break;
        case 'usrName':
            i = 1;
            break;
        default:
            i = 0;
            break;
        }
      
    return i;
}

// export  const colTemplate = ['schAppVersionCode','schName','stdinfo','featureErrGroup','stdMessage','stdDevice'];
const colTemplate = ['appVerCode','school','usrName','errGroup','errMsg','stdDevice'];
// console.log(typeof(jsondata));


export function parseJSONtoStructuredData(jsondata){
    const structuredData = []
    jsondata.map((issue,i)=>{
        const dataRow = {};
        // console.log("Issue :",issue);
        issue.blocks.map((section,i)=>{
            // console.log("Section : ",colTemplate[i]);
            // console.log("Block Id : ",section.block_id)
            if(section.text.text.toString().includes('|')){
                // console.log('Parts : ', section.text.text.toString().split('|'));
                dataRow[colTemplate[i]] = section.text.text.trim().toString().split('|')[getIndexByColName(colTemplate[i])];
                // dataRow[colTemplate[i]] = section.text.text;
    
                if(colTemplate[i]==='usrName'){
                    dataRow[colTemplate[i]] = section.text.text.trim().toString().split('|')[getIndexByColName(colTemplate[i])] +'|'+ section.text.text.trim().toString().split('|')[getIndexByColName(colTemplate[i]) + 1]
                }
            }else{
                // console.log("Content : ",section.text.text);
                dataRow[colTemplate[i]] = section.text.text.trim();
            }
        return 0;
        })
        // console.log("dataRow ============= : ", dataRow)
        dataRow.key = (i+1).toString();
        dataRow.status = 'Pending'
        structuredData[i] = dataRow;
    return 0;
    })

    return structuredData;
}



// console.log(colTable1)

// console.log(structuredData);

export function getIssueCount(data){
    var issuesList = []
    data.map((val,i)=>{
        issuesList[i] = val.errGroup;
        return 0;
    })
    issuesList = issuesList.filter((v, i, a) => a.indexOf(v) === i); 
    return issuesList;
}
export function getUserCount(data){
    var userList = []
    data.map((val,i)=>{
        userList[i] = val.usrName.split("|")[0].trim();
        return 0;
    })
    userList = userList.filter((v, i, a) => a.indexOf(v) === i); 
    return userList;
}
export function getVersionCount(data){
    var versionList = []
    data.map((val,i)=>{
        versionList[i] = val.appVerCode;
        return 0;
    })
    versionList = versionList.filter((v, i, a) => a.indexOf(v) === i); 
    return versionList;
}

export function getIssueCountByName(data,k){
    var count = 0;
    data.map((v,i)=>{
        count += v.errGroup.toLowerCase().trim() === k.toLowerCase().trim() ? 1 : 0;
        return 0;
    })

    return count
}

export function getIssueStatusByName(data,issueName){
    console.log("Args : ", data,issueName)
    return 'hello'
}

export function getStatusColor(status){
    let color = 'Black';
    console.log("Status In color: ",status)
    switch(status){
        case 'pending':
            color = 'gold';
            break;
        case 'solved':
            color = 'green';
            break;
        case 'discarded':
            color = 'red';
            break;
        default:
            color = 'Black';
            break;
    }
    return color;
}