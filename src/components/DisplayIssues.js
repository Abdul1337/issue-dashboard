import React from 'react';
import 'antd/dist/antd.css';
import { Table,Button, Collapse, Descriptions, Tag, Modal, Tooltip } from 'antd';
import {InfoCircleOutlined, EditOutlined, EllipsisOutlined } from '@ant-design/icons';


import {sortByKey,
    getIssueCount,
    getIssueCountByName,
    getIssueStatusByName,
    getVersionCount,
    getStatusColor,
} from './Utils'


import './main.css'

const colIssueTable = [
    {title: 'S.N.', dataIndex: 'key',key: 'key',render: sn => <strong>{sn.toString()}</strong>,},
    {title: 'Error Group', dataIndex: 'errGroup',key: 'errGroup',width : 200,sorter : sortByKey.bind({key :'errGroup'}),},
    {title: 'Error Message', dataIndex: 'errMsg',key: 'errMsg',width : 400,
        render: msg => <i style={{cursor : 'pointer'}} >{msg.toString().trim().length > 35 ? shortenTextAsTooltip(msg,35) : msg.toString().trim()}</i>
    },
    {title: 'App Version/Code', dataIndex: 'appVerCode',key: 'appVerCode',width : 150,sorter : sortByKey.bind({key :'appVerCode'}),},
    {title: 'School', dataIndex: 'school',key: 'school',sorter : sortByKey.bind({key :'school'}),render: schoolName => <span>{schoolName.toString().split("# ")[1]}</span>,},
    {title: 'User Name', dataIndex: 'usrName',key: 'usrName',width : 250,sorter : sortByKey.bind({key :'usrName'}),render: userName => <span><strong>{userName.toString().split('|')[0]}</strong><br/>[{userName.toString().split('|')[1]}]</span>,},
    {title: 'Status', dataIndex: 'status',key: 'status',
        render: (status) => (
            <div style={{display:'flex'}}>
                <Tag color={getStatusColor(status.toLowerCase())} style={{color : 'red'}}>{status.toString()}</Tag>
                <StatusEditor/>
            </div>
        ),
    },
];

const shortenTextAsTooltip = (msg,len) =>{
    return <Tooltip title={msg} color='blue'>{msg.trim().slice(0,len)}<EllipsisOutlined /></Tooltip>
}



const StatusEditor = (props)=>(
    <div>
    <Tooltip title="Edit Issue Status">
        <Button  >
            <EditOutlined/>
        </Button>
    </Tooltip>
    </div>

)


class DisplayIssues extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            issuesName : this.getIssueListSorted(this.props.data),
            modalVisible: {},
            data : this.props.data,
        };
    }
    toogleModleVisiblity = (visiblity,i) => {
        let mv = this.state.modalVisible;
        mv[i] = visiblity;
        // console.log(mv,this.state.modalVisible)
        this.setState({
          modalVisible: mv,
        });
      };

    getIssueListSorted = (data) =>{
        let issuesSrtByCount = getIssueCount(data).sort(function(a,b){
            let ac = getIssueCountByName(data,a);
            let bc = getIssueCountByName(data,b);
            return bc - ac;
        });
        return issuesSrtByCount;
    }
    render(){
        console.log("Modal Table Data : ", this.props.data)
        const { Panel } = Collapse;
        return(
            <div>
            <div className="TableTitle">{this.props.title}</div>
            <div className="dataDisplay" >
                  <Collapse accordion>
                    {
                        this.state.issuesName.map((v,i)=>(
                            <Panel header={v.toUpperCase()} key={i}>
                                <Descriptions title="Issue Details">
                                    <Descriptions.Item label="Issue Group">{v}</Descriptions.Item>
                                    <Descriptions.Item label="Reported Count">{getIssueCountByName(this.state.data,v)}</Descriptions.Item>
                                    {/* <Descriptions.Item label="Status"><Tag color="gold">{getIssueStatusByName(this.state.data,v)}<EditOutlined /></Tag></Descriptions.Item> */}
                                    <Descriptions.Item label="App Version(s)">{getVersionCount(this.state.data).join(",")}</Descriptions.Item>
                                    <Descriptions.Item label="See List"><Button type="link" onClick={()=>{this.toogleModleVisiblity(true,i)}}>Click Here<InfoCircleOutlined /></Button></Descriptions.Item>
                                </Descriptions>
                                
                                <Modal
                                key={i}
                                title={v.toUpperCase()}
                                visible={this.state.modalVisible[i]}
                                onOk={()=>{this.toogleModleVisiblity(false,i)}}
                                onCancel={()=>{this.toogleModleVisiblity(false,i)}}
                                width = {1500}
                                >
                                    {/* <Displaydata title={"Submissions"} data={this.props.data.filter(sub=>sub.errGroup.trim() === v.trim())} columns={colIssueTable}/> */}
                                    <Table 
                                        dataSource={this.props.data.filter(sub=>sub.errGroup.trim() === v.trim())}
                                        columns={colIssueTable}
                                        // pagination={{ pageSize: 100 }} 
                                        // scroll={{ y: 500 }} 
                                    />
                                </Modal>
                            </Panel>
                        ))
                    }
                </Collapse>
            </div>
            </div>

        )
    }

}

export default DisplayIssues;